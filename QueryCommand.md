Did you ever feel the need for some addressbook "better" than mutt's
internal alias management?

By setting **$query\_command** in your .[muttrc]([ConfigList](ConfigList) "wikilink")
you can tell mutt to use an external program for looking up addresses
when invoking the "Query" command (hit '?' for mutt-keybinding).
**query\_command** is used as well for address completion in any prompt
for address entry, default by hitting ^T.

### Selecting multiple lines in query menu

The query command can return multiple addresses. If you want to select
multiple addresses it does not work if you tag them with 't' and then
hit **return**. What works for me is pressing **;-m**.

### Different tools for queries

* <http://www.bsdconsulting.no/tools/> -- mutt-ldap.pl  
 * Query Active Directory & Exchange for mail addresses using LDAP.

* <http://abook.sourceforge.net/> -- Jaakko Heinonen
* <http://home.uchicago.edu/~dgc/mutt/#qi> -- David Champion (dgc)  
 * mutt-phquery -- directory lookups in Qi

* <http://www.spinnaker.de/lbdb/> -- The Little Brother's Database is capable of extracting addresses from various sources including messages fed individually by a MDA or bulk fed, for example in the way described at <http://serendipity.ruwenzori.net/index.php/2006/02/25/feeding-whole-maildirs-to-lbdb-for-mutts-address-completion>

* <http://shove-it.de/open/jcm/muttquery.py> --   
 * I searched the web to find a query program to query the kde-addressbook. I use kaddressbook because its doesn't give me carpal tunnel. This script just reads the std.vcf file. No need for an addressbook-conversion. Please write any improvement requests here. 

* [QueryCommand/MuttLdapSearch](QueryCommand/MuttLdapSearch) -- Search LDAP directory using only ldapsearch and SH commands.  
 * Use this if you want a query_command for LDAP with as few requirements as possible. Just copy and paste into your favorite text editor (be sure to check the lines ending in '\' to make sure there are no extraneous spaces at the end) and save as 'mutt_ldap_search.sh' or whatever you prefer. Be sure to make it executable (chmod a+x mutt_ldap_search.sh) and add an appropriate query_command line to your .muttrc file.
* [QueryCommand/EvolutionSearch](QueryCommand/EvolutionSearch) -- Search the Ximian Evolution 2 address book.  
 * Use this if you want a query_command for searching your Evolution address book. It has not been extensively tested but is simple and should be easy to customize.

* Gmail contact search  
 * [goobook](http://gitorious.org/goobook/) - cjbarroso  
 * [gmailcontacts.py](https://github.com/redondos/gmailcontacts/) - Angel Olivera (redondos)  
 * [mutt_gmail_search.py](http://git.chizang.net/?p=mutt_gmail_search.git;a=tree) - alex@chizang.net
* <http://sourceforge.net/projects/abtomutt/files/> -- Address Book to Mutt (MacOS X)  
 * This program's sole function is to perform lookups in the MacOS X Address Book and feed the results to the terminal in a format that Mutt can understand for e-mail address queries and e-mail address completion.

### Using Multiple Query Tools

The above tools are great if all your addresses are in one place, but
what if you've got multiple sources to search? (My need for a solution
for this came from having to search the corporate LDAP directory and
abook's records.)

Fortunately, many \*nix distributions (Debian, Redhat both confirmed)
come with the perfect tool for combining these resources. Additionally,
this method can be used with the majority of the tools listed above and
with most stand-alone scripts written for mutt on the internet:

    user@myhost:~/.mutt/address-providers$ pwd
    /home/user/.mutt/address-providers
    user@myhost:~/.mutt/address-providers$ ls -l
    total 8
    -rwx------ 1 user user  127 2008-04-16 22:43 dummy_addresses
    -rwx------ 1 user user 1708 2008-04-16 18:58 mutt_ldap

```sh
    user@myhost:~/.mutt/address-providers$ cat dummy_addresses
    #!/bin/bash
    echo "hello@hello.com  buuuuuuuuuh     HELLOOOOOOOOOOOOOOO"
    echo "gfdgfd@example.net Roger Smith     Properly Formed Entry"
    user@myhost:~/.mutt/address-providers$ grep query ~/.muttrc
    set query_command = "echo ; /bin/run-parts -a '%s' /home/user/.mutt/address-providers/|grep '@'"
```

To add a new search mechanism, just drop the file into the
address-providers directory. Each script in address-providers should

1. be executable  
1. have a name containing only these characters: [a-zA-Z0-9-_]  
1. take a query as its first parameter

As a result of (2), to stop any script from being used just pop a period
in its name: "mv Broken\_Script Broken\_Script.bother"

NB1 In my example, the "dummy\_addresses" provider is there simply to
prove to yourself that the mechanism is working. Remove it after
testing.

NB2 A couple of points about the
query\_command:

1. The initial "echo;" is there because mutt doesn't use or display the first line as a potential address; it doesn't seem to be used anywhere with my fairly stock mutt install, but that doesn't mean it's useless.  Change the echo statement to output anything you want  
1. The final "grep '@'" is a nasty way of removing each script's initial reporting line that mutt would normally remove itself, as per point (1).  It works as long as you're only looking for addresses that definitely use @'s, and you don't need each script's reporting line, and no reporting line contains an @.  For most people, none of this is a problem - just use it as is.  If you've got a problem with this, then you probably know enough to alter the command to achieve what you want ... -- jaycee
