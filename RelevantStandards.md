This page contans a list of relevent IETF standards that concern the
tranmission and handing of email
messages.

* RFC1524, A User Agent Configuration Mechanism For Multimedia Mail Format Information.  
* RFC1939, Post Office Protocol - Version 3.  
* RFC2045, Multipurpose Internet Mail Extensions (MIME) Part One: Format of Internet Message Bodies.  
* RFC2046, Multipurpose Internet Mail Extensions (MIME) Part Two: Media Types.  
* RFC2047, MIME (Multipurpose * Internet Mail Extensions) Part Three: Message Header Extensions for Non-ASCII Text.  
* RFC2087. IMAP Quota Extension  
* RFC2156, MIXER (Mime Internet X.400 Enhanced Relay): Mapping between X.400 and RFC 822/MIME  
* RFC2177, IMAP IDLE Command  
* RFC2183, Communicating Presentation Information in Internet Messages: The Content-Disposition Header Field.  
* RFC2279, UTF-8, a transformation format of ISO 10646.  
* RFC2231, MIME Parameter Value and Encoded Word Extensions: Character Sets, Languages, and Continuations.  
* RFC2369, The Use of URLs as Meta-Syntax for Core Mail List Commands and their Transport through Message Header Fields.  
* RFC2822, Internet Message Format  
* RFC3156, MIME Security with OpenPGP.  
* RFC3501, IMAP (Internet Message Access Protocol) - Version 4rev1  
* RFC3676, The Text/Plain Format and DelSp Parameters (a.k.a Format=Flowed).  
* RFC4314, IMAP4 Access Control List (ACL) Extension  
* RFC4466, Collected Extensions to IMAP4 ABNF  
* [The Maildir
format](http://cr.yp.to/proto/maildir.html)  
* [Maildir++](http://www.inter7.com/courierimap/README.maildirquota.html)

A more complete list can be found at: <http://www.tin.org/docs.html>.
