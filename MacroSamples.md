Mutt is powerful because you can
combine atomic functions to build complex functionality. One such
combining is provided by macros. For those too lazy to reinvent the
wheel(s), let's collect some examples either for usefulness or
educational purposes. *See also [MuttGuide/Macros](MuttGuide/Macros).*  
**Note:** many macros for either "index" or "pager" mode are often
useful for the other mode, too, when they deal with a single selected
message. So keep in mind to define those for both modes to avoid to
wonder why something doesn't work where you expect it.

-----

## Change to a path prefix; complete and confirm with enter

 ```macro index Aw <change-folder>imaps://user@imap.host/```

## Pass HTML attachment to lynx

(see also
"`wait_key`")

` macro pager l "<pipe-entry>lynx -stdin -force_html<enter>"`  
` macro attach l "<pipe-entry>lynx -stdin -force_html<enter>"`

## "poor man's" trash-folder management

This does work with `<tag-prefix>` to apply for multiple, so seems a
pretty good substitute for the trash folder
patch:

` folder-hook .      'macro index d "<save-message>=trash<enter>"'`  
` folder-hook =trash 'macro index d <delete-message>'`

## "somewhat wealthier man's" trash-folder management

\-- at least flaky if your inbox is not a Maildir; adjust "inbox" as
necessary:

    set maildir_trash=yes
    set wait_key=no
    folder-hook . 'bind index q quit'
    folder-hook inbox 'macro index q ":unset maildir_trash;push \"T~D\\n<tag-prefix-cond>m=trash\\n<end-cond><quit>\"\n"'

## if your trash folder-name has spaces in it

` folder-hook .      'macro index d "<save-message>=Deleted<quote-char> Items<enter>"'`  

## Training Spamassassin

(be sure to modify spam destination, esc-d for spam, esc-u for unspam,
esc-t to test) See also \`pipe\_decode +
    pipe\_split\`!!!

    macro index \ed "<enter-command>unset wait_key\n<pipe-entry>sa-learn --spam\n<enter-command>set wait_key\n<save-message>=spam/evilspam\n" "Tags as SPAM"
    macro index \eu "<pipe-entry>sa-learn --ham\n<enter-command>set wait_key\n<save-message>=inbox\n" "Untags as SPAM"
    macro index \et "<pipe-entry>spamassassin -t\n<enter-command>set wait_key\n" "Tests if it is SPAM"

## Toggle ROT13 decoding with ESC r.

Principle explained in
    [ConfigTricks](ConfigTricks)

    macro pager ,@r13on "<enter-command> set display_filter=/usr/local/bin/rot13; macro pager \\er ,@r13off 'Toggle ROT13 decoding'<Enter><exit><display-message><enter-command> set ?display_filter<Enter>"
    macro pager ,@r13off "<enter-command> unset display_filter; macro pager \\er ,@r13on 'Toggle ROT13 decoding'<Enter><exit><display-message><enter-command> set ?display_filter<Enter>"
    macro pager \er ,@r13on 'Toggle ROT13 decoding'
