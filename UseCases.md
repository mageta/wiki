This is the place to share your experiences with mutt about the first
steps and/or about some very hard lessons for specific uses, rather than
the functionally complete overview of [MuttGuide](MuttGuide).

You decide, just keep updating.(Eventually the knowledge will be
digested into the [MuttGuide](MuttGuide) as resources become
available.)

-----

local:

* [UseCases/MultiAccounts](UseCases/MultiAccounts): How to set up mutt to automatically select the correct account when replying to messages.  
* [UseCases/SearchingMail](UseCases/SearchingMail): Using mail indexers to search mail from within mutt  
* [UseCases/Gmail](UseCases/Gmail): accessing your GMail account via IMAP

-----

external:

* <http://mutt.blackfish.org.uk/> -- My first mutt, quick beginners guide for anybody adopting the mutt mailer  
* <http://www.melonfire.com/community/columns/trog/article.php?id=98> -- A Man And His Mutt - A tutorial on installing, configuring and using Mutt.  
* <http://www.ucolick.org/~lharden/learnmutt.html> -- A simple & quick one  
* <http://ataualpa.altervista.org/muttnirvana/> -- "Il Nirvana con Mutt": a complete guide in **italian**, structured by actions for easier learning, a lot of examples  
* <http://codesorcery.net/old/mutt/mutt-gnupg-howto> -- Justin R. Miller, using Mutt with GPG  
* <http://koweycode.blogspot.com/2006/10/getting-things-done-with-mutt.html> -- Getting Things Done with mutt -- tips and ideas (mostly ideas) for using mutt with GTD  
* <http://www.calmar.ws/mutt/> some first steps to think about.
