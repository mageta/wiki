### How to save all attachments into the same directory, easily?!

Mutt 1.5.13 (2006-08-11):

View attachments, tag the ones you want to save, then use "`tag-save`"
("`<tag-prefix><save-entry>`"). Enter the *directory to save under* (no
filename) and you'll be prompted: "File is a directory, save under it?
\[(y)es, (n)o, (a)ll\]". The "a" option does what you want it to, and
you get a chance to approve each save individually as well (in case you
want to change a filename here or there).

Another solution that will work:

Quit mutt, then start mutt in the directory you want to save them, then
enter "view attachments" mode, "tag" them all and then save them. See
'?' on-line help for the keys to the functions.

Or define these
macro:

```
macro attach W <save-entry><kill-line>/home/gawron/attachments/  
macro attach E <save-entry><kill-line>/home/gawron/attachments/<enter>
```

This will either save automatically (E) to a given dir with a single
keystrike, or (W) give you a default path to either confirm or expand
and then to save. As with all [MuttGuide/Macros](MuttGuide/Macros), they can't be used with
tga-prefix.

### How to use different mailcap entries based on whether X is running:

The mailcap file has a "test=" field. If the part after the "="
evaluates to false, then the line is skipped and the program reading
.mailcap searches for a subsequent line with a matching MIME type. This
can be used for lots of slick stuff, but the focus here is whether X is
running and accessable. To check that, and easy method is to see if the
$DISPLAY variable is set:

`test=test -n "$DISPLAY"`

Don't be confused by the second "test," it's just one alias of the "\["
program, so the command 'test -n "$DISPLAY"' is the same as '\[ -n
"$DISPLAY" \]'.

An example with text files then would be to add the following lines to
your
~/.mailcap:

``` 
 # If $DISPLAY is set, open it in 'xless,' the X11 version of classic 'less'
 text/*; xless '%s'; test=test -n "$DISPLAY"
 # If it's not set, we can't use X, so fall back on the usual console pager, 'less'
 text/*; less '%s'
```

Only one "test=" field is allowed per line, so every association on a
line is linked to the "success" of that field. The next example uses
this to define numerous fields for each of the X running/X not running
lines:

``` 
 #If $DISPLAY is set, use X-based counterparts
 text/html; mozilla %s; test=test -n "$DISPLAY"; edit=gvim; compose=gvim; nametemplate=%s.html
 #Fallback if X isn't available
 text/html; w3m -v -F -T text/html %s; edit=vim; compose=vim; nametemplate=%s.html; needsterminal
```

### How to view/ reply HTML, images and other non-textual attachments?

Please read carefully
[section 5](https://muttmua.gitlab.io/mutt/manual-dev.html#mimesupport) of the mutt
manual, in particular 5.3.1 mailcap basics and 5.4!  
Once you got that, check out the [UserPages](UserPages) for help applications to
display various data formats, especially Dave Pearson's.

in mailcap:

``` 
 ##this is used when (v)iewing a text/html attachment:
 ##allowing you to browse and "click" links
 ##order matters the first entry which passes the test is used
 #gui browser if $DISPLAY is set
 text/html; mozilla %s; test=test -n "$DISPLAY"; nametemplate=%s.html
 #text browser otherwise
 text/html; w3m -v -F -T text/html %s; nametemplate=%s.html; needsterminal

 ##auto_view will use the entry with the copiousoutput part:
 text/html; lynx -stdin -dump -force_html ; copiousoutput
```

in muttrc:

``` 
 #if there is only html dump that
 auto_view text/html

 # this helps with multipart messages
 alternative_order text/plain text/html
```

Note: Mutt exports text/\* attachements in the character set/encoding
used by the terminal it runs on. By using text/html attachements, there
is a problem with this that needs to be circumvented: the html
attachement can contain a key for specifying the character encoding of
the html text, which is not neccessarily in accordance with the terminal
encoding (the sender of the email does not know, what encoding mutt on
the recieving end uses). Thus viewing these attachements with text
encoded in the terminal encoding will fail. Work arounds:

1.  tell the html-to-text converter (e.g. lynx) to ignore the html tag
    specifying the encoding (as done above)
2.  re-encode the file using the correct character set. This can be done
    by a script using iconv [Bugreport with attached
    script](https://dev.mutt.org/trac/ticket/3415)

For images you can use "aview" from the "aalib" project:
<http://aa-project.sourceforge.net/>

However, you'll need to patch it to use it as filter to display with
mutt's built-in pager.

### How to view HTML attachments in a new tab in firefox:

Assuming you already have a main firefox window running, when you invoke
"firefox -new-tab %s", it does not block. It simply notifies your main
running firefox process to open a tab pointing to %s and then exits
right away. There is now a race condition: does mutt delete the temp
file before or after your main firefox process creates the new tab
pointing to it?

There is no completely clean way to fix this, because there is no way to
know how long the main firefox will need %s to be around. If you only
intend to quickly view the file and then close the tab, one solution is
to have mutt sleep after invoking "firefox -new-tab".

in mailcap:

``` 
    text/html; firefox -new-tab %s & sleep 5; test=........
```

This will usually give firefox enough time to read the temp file in
before the sleep ends and mutt deletes the file.

### How to make M$-office attachments accessible?

Uh, oh, better don't try but convince people to use portable formats. :)

Seriously, if you really must, use something like openOffice or some
other simpler [MuttTools](MuttTools) like antiword, or have a look at Gary Johnson's
[UserPages](UserPages).

In 1.5.X there is a new setting that helps with application/octet-stream
attachments. <i>mime\_lookup</i>. See [the
manual](https://muttmua.gitlab.io/mutt/manual-dev.html#mime-lookup)

`mime_lookup application/octet-stream`

Everybody should set this.

### How do I forward a message including attachments?

Add

`set mime_forward=yes`  
`set mime_forward_rest=yes`

to your **~/.muttrc**. This will cause Mutt to attach the message rather
than quoting it in the body of the message.  
Otherwise use
"bounce".

### But I want to add my own comments to the top of the forwarded message, how to do that?

Go to the attachment-menu, tag all attachments you want to forward and
also tag the text part of the e-mail. Then invoke "tag-prefix" +
"forward" (by default press ";f", hit "?" for your
keymap).

### How to make mutt use a correct content-type (data-format) for attachments I send out?

Have a look at section 5 of the [manual](https://muttmua.gitlab.io/mutt/manual-dev.html#mimesupport). In
particular learn about how to setup a proper **mime.types** file. With
that you must tell which files are assigned which MIME types/
formats.

### Can mutt stop me from making me look stupid by sending mails saying "patch attached" without attachments?

Yes! :) See [ConfigTricks/CheckAttach](ConfigTricks/CheckAttach).

### Why can't I see attachments in some multipart/alternative emails?
Some email clients incorrectly bundle their attachments in one of the
alternatives. Instead of sending

    [multipart/alternative]
      -> [text/plain]
      -> [text/html]
    [application/pdf]

they send something like

    [multipart/alternative]
      -> [text/plain]
      -> [multipart/mixed]
        -> [text/html]
        -> [application/pdf]

A workaround is to put multipart/mixed and multipart/related types
before text/plain in your `alternative_order` list:

    unalternative_order *
    alternative_order multipart/mixed multipart/related text/plain
