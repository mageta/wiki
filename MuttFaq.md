Welcome to the Mutt FAQ!

- Read The Fine Manual(RTFM), please.
- *Read [manual.txt](http://www.mutt.org/doc/manual.txt) for solutions
  to your problems!* Relevant answers to your questions are in the
  manual.
- Wiki-wide search is available using the wiki-search box. Page search
  by page using your browser 'search' function.
- Use [DebugConfig](DebugConfig) as a starting point to analyze unusual problems.
  Share your answers here.
- Read the [WikiWorker](WikiWorker) guidelines *before* you edit!

- Most Frequently Asked Questions & Answers  
  *       Where is the Mutt manual? - [here](http://www.mutt.org/doc/manual.txt) or look for "manual.txt" on the [MuttWiki](home) page.  
  *       I'm having problems after an upgrade and my old muttrc is giving me errors - read the !ChangeLog in source package for changed features and new manual.txt passages for problem items.  
  *       I'm having folder problems, using only IMAP folders mutt still queries about local folders, why? - see [MuttFaq/RemoteFolder](MuttFaq/RemoteFolder)  
  *       I'm having character set issues and can't see character X in latin1/windows-1256/whatever-encoded messages - see [MuttFaq/Charset](MuttFaq/Charset)  
  *       I want to have Mutt to use a remote SMTP server to send mail - see [MuttFaq/Sendmail](MuttFaq/Sendmail)  
  *       I want to show the (new) message count in the folder browser - see [MuttFaq/Display](MuttFaq/Display)  
  *       I want some good mutt config files (muttrc) - see [ConfigList](ConfigList) + [ConfigTricks](ConfigTricks)  
  *       I want to make my mutt config dynamic (with "if"-conditions) even without [ScriptSupport](ScriptSupport)? - see [MuttFaq/Misc](MuttFaq/Misc)  
  *       I want to find the reason for muttrc errors see [DebugConfig](DebugConfig)  
  *       I have two identical configs on different machines, but one fails - see [DebugConfig](DebugConfig)

- [MuttFaq/Action](MuttFaq/Action) (interactive)  
  *      What's the key (-binding) for function XYZ?  
  *      How do I "move" or "save"?  
  *      How do I save / delete / copy / print / pipe multiple or several messages?  
  *      How do I search / tag / match / view only specific messages?  
  *      How do I mark a message as "read"?  
  *      How do I actually delete messages marked for deletion or save read messages to "mbox"es without having to quit?  
  *      How do I use buttons with my mouse!?  
  *      How do I see the "mailboxes" folders with new mail as with "-y" option without restarting mutt?  
  *      How do I create a new folder?  
  *      How do I stop at the end of a message when paging down and not jump to next one?  
  *      Why don't shift or control cursor keys work? 

- [MuttFaq/Header](MuttFaq/Header)  
  *      How do I set my From: address?  
  *      How do I set the address used in the SMTP negotiation (envelope address)?  
  *      How do I fix my local hostname when I've set my From: header field in Mutt, but the SMTP server still sees it?   
  *      Why does a remote SMTP server refuse to accept my email because of a wrong domain name, even if I gave a correct From:?  
  *      How do I manage several different roles (From:-addresses)?  
  *      How do I let mutt use the To: address of a message as a From: address in a reply?  
  *      How do I fix set $reverse_name, when it's not working?  
  *      How do I change "Message Priority" for outgoing mail? 

- [MuttFaq/Folder](MuttFaq/Folder)  
  *      What does "XYZ is not a mailbox" or "has no messages" mean?  
  *      Why are "new" flags of mbox folders wrong (folder-list view, not single messages in index view)?  
  *      How do I make mutt open a specific folder on start up?  
  *      How do I save copies of outgoing / sent email? How to do it for the current folder?  
  *      Why can't I modify my inbox folder. It says its read-only but my permissions are ok?  
  *      How do I make mutt check for new mail more often? What's the difference between $timeout and $mail_check?  
  *      How do I make mutt stop asking to "move read messages to mbox"!  
  *      How do I use the "trash" folder?  
  *      How do I import messages from Pine, Thunderbird, Kmail, Evolution, ...?  
  *      How do I fix a missing trailing '/' in my .procmailrc, and procmail delivered many emails to a Maildir folder in MH format?  
  *      What's the difference between '+' and '='? 

- [MuttFaq/RemoteFolder](MuttFaq/RemoteFolder)  
  *      How do I access my POP/IMAP account(s)?  
  *      When using a ssh-tunnel-to-IMAP I get error messages, but the tunnel commands work well in a shell, why?!  
  *      How do I save typing long IMAP-pathnames?  
  *      How do I list/ browse remote IMAP-folders?  
  *      How do I fix mutt queries about local folders when I'm using only IMAP folders?  
  *      How do I fix being disconnected when I leave mutt alone for a few minutes  
  *      How do I fix having to review my server's SSL certificate every single time I connect?  

- [MuttFaq/Sendmail](MuttFaq/Sendmail)  
  *      Why do I get "Error sending message, child exited"?  
  *      How do I discover why mail doesn't arrive or bounces?  
  *      How do I discover why sending email is slow?  
  *      How do I configure Mutt to use a remote SMTP server to send mail?  
  *      How do I use different SMTP servers for several accounts?  
  *      How do I save retyping my password when SMTP authentication (APOP etc.) times out on me? 

- [MuttFaq/Appearance](MuttFaq/Appearance)  
  *      How do I split the screen to see index and pager / body / content all at once?  
  *      How do I use the "default" color of my terminal or the "transparent" color?  
  *      Why is "default" not recognized when mutt with ncurses [5.2] starts up?  
  *      Why Using a "default" background does not work.  
  *      Why doesn't the background highlight color show behind white space?  
  *      Why, when running in screen, transparency does not work, and there are trailing spaces filling every line when cut-and-pasting  
  *      What is the mutt color equivalent of 'grey'?  
  *      How do I have an arrow ('>') as an indicator?  
  *      How do I highlight certain email addresses in my tree? 

- [MuttFaq/Display](MuttFaq/Display)  
  *      How do I change the format/display of the list of mailboxes (or any menu)?  
  *      How do I personalize / shortcut the display of or label the folder names in the folder browser?  
  *      Why doesn't $index_format "%b" show some Maildir?  
  *      Can Mutt show the (new) message count in the folder browser?  
  *      How do I make the From-addr show normally in index-view or show To-addr for mail that I send?  
  *      Can mutt use virtual folders / mailboxes?  
  *      Why are some 'N'(ew) messages flagged 'O'(ld) when I leave them and return? 

- [MuttFaq/Attachment](MuttFaq/Attachment)  
  *      How do I easily save all attachments into the same directory?!  
  *      How do I use different mailcap entries based on whether X is running (X availability-dependent associations for MIME types)?  
  *      How do I view/ reply HTML, images, vcards and other non-plain-text attachments?  
  *      How do I view HTML attachments in a new tab in Firefox?  
  *      How do I make M$-office attachments accessible?  
  *      How do I forward a message including attachments?  
  *      How do I add my own comments to the top of the forwarded message?  
  *      How do I make mutt use a correct content-type (data-format) for attachments I send out?  
  *      How do I make mutt stop me from making me look stupid by sending mails saying "patch attached" without attachments? 

- [MuttFaq/Charset](MuttFaq/Charset)  
  *      Umlauts, accents, and other non-ASCII characters are displayed as '?' or '\123', fine in some mails, but hidden in others.  
  *      Umlauts, accents, and other non-ASCII characters are only displayed wrong when using auto_view  
  *      How do I check if locales work before I blame Mutt for it?  
  *      UTF-8 chars are displayed fine, but the screen is garbled  
  *      How do I fix messages still being garbled even after I tuned all the variables correctly?  
  *      Why are attached text files sent incorrectly encoded with the wrong charset. 

- [MuttFaq/Encoding](MuttFaq/Encoding)  
  *      Why is a message not printed as seen in mutt?  
  *      Why is a message not caught by spam defense when piped to it?  
  *      How do I launch a browser with text links in plain text but encoded emails?  
  *      How do I search encoded messages like quoted-printable ones? 

- [MuttFaq/Alias](MuttFaq/Alias)  
  *      How do I use the aliases (addressbook)?  
  *      How do I fix changed aliases that mutt still doesn't seem to know?  
  *      How do I find a no better way to manage often used addresses? 

- [MuttFaq/Encryption](MuttFaq/Encryption)  
  *      How do I make PGP/GPG work?  
  *      How do I make old style / classic / traditional / inline PGP work?  
  *      Why do other MUAs (mail-programs) have problems handling mutt's PGP-stuff being attachments?  
  *      How do I fix typing the wrong passphrase when mutt keeps using it?  
  *      Does Mutt do S/MIME?  
  *      Why can't I read my own postings when using gpg to encrypt messages?  
  *      Which key-server should I use?  
  *      How do I retrieve PGP-keys automatically? 

- [MuttFaq/Thread](MuttFaq/Thread)  
  *      How do I get rid of duplicate messages?  
  *      How do I change the order within a thread?  
  *      How do I sort threads based on all the messages in the thread?  
  *      Why are some messages threaded and others are not?  
  *      What do "->", "-?-" and "*>" mean in thread trees?  
  *      Why is the tree display screwed, I see garbage characters in front of the arrows?! 

- [MuttFaq/Maildir](MuttFaq/Maildir)  
  *      How do I search multiple Maildirs for strings all at once?  
  *      Which mailbox format is better, mbox or Maildir?  
  *      What is the best directory layout for Maildir folders?  
  *      How do I create sub-folders?  
  *      How do I access the sub-folders created by Kmail and Courier-IMAP?  
  *      I can see size in bytes, but not lines, is there a fix? 

- [MuttFaq/Editor](MuttFaq/Editor)  
  *      Why are there strange files (*mutt*~ or *mutt*.bak) when mutt finishes?  
  *      How do I configure Mutt to use mail-mode in Emacs?  
  *      How do I trim quoted replies (like stripping signatures)? 

- [MuttFaq/Hook](MuttFaq/Hook)  
  *      My settings are messed up after hooks are executed, why are they not restored?  
  *      Why get the commands given in hooks crippled, some are skipped?  
  *      Why does my muttrc errors for commands specified in hooks at start up?  
  *      How do I make a complex regular expression containing "(|)" and "\" in a *-hook/search work?  
  *      How do I fix my macro, after I already double quote stuff for patterns, and it still doesn't work? 

- [MuttFaq/Misc](MuttFaq/Misc)  
  *      What is mutt's way to provide "templates" for sending new email?  
  *      How do I make random signatures?  
  *      What am I missing when I invoke mutt in batch mode from the command-line but it goes interactive?  
  *      How do I access the development versions of Mutt?  
  *      Where do I find the change log of the differences/features between releases (stable, dev)?  
  *      My Mutt config is static, how to make it dynamic (with "if"-conditions) even without [ScriptSupport](ScriptSupport)?  
  *      What is the meaning of "mbox", "maildir", and "folder"?  
  *      How do I fix error 'Could not copy message'?  
  *      Switching folders is too slow with big folders, how do I speed it up?  
  *      How do I trigger a script on new mail?   
  *      How do I help update the mutt.org site?

-----

If you don't find a suitable question (or answer), add it here below the
line to be dealt with by some sage.

-----
